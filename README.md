
# Introduction
IC-over-Netio tool is a tool is a mini-software library that was designed for FELIX hardware, that allows IC commands to be sent over felixcore (netio) for LPGBT I2C coumminication.
This software is based on the NSW project's ic-over-netio tool but is heavily modified. 
The software is written in C++ and can be used as a c++ class or a python module. 


# Compile Instructions 
The package dependencies are kept at a minimum only PYBIND11 and TBB packages are required
For machines with CVMFS acceses you should be able to compile this package out of the box following these instructions

```
git clone ssh://git@gitlab.cern.ch:7999/itk-felix-sw/itk-felix-sw.git
cd itk-felix-sw
git clone ssh://git@gitlab.cern.ch:7999/itk-felix-sw/ic-over-netio.git
source setup.sh
mkdir build
cd build
cmake ../
make -j install
```

For non CVMFS machines, either TDAQ package can be installed or a custom CMAKE file could be written as the dependencies are super small.  

# How to use
To use the software you just need to source the itk-felix-sw by calling
```
cd itk-felix-sw
source setup.sh
```

Then you can checkout the use example on the ic-over-netio/test.py example. 
Main idea is you connect to a given Felix server in a given channel using (Python): 
```
from libic_comms import *
IC = IChandler("127.0.0.1", 12340, 12350, 17+64, 29+64, False, 0, 0x70)
IC.readCfg()
```
where the individual fields are:
```
IChandler(<IP of FELIX Machine>,
	  <TX PORT>, 
	  <RX PORT>, 
	  <Elink id for IC CMD  generaly 17+64xFiberID>,
	  <Elink id for IC Data generaly 29+64xFiberID>	,
	  <Attempt to re-subscribe if communication fails>,
	  <LpGBT Version, currently 0,1 is supported>,
	  <I2C identified of the MASTER hadrware>
```

Then individual IC commands can be sent to

1. read all LpGBT registers at once
```
IC.readCfg()
```

2. write all LpGBT registers at once
```
IC.sendCfg(<Array of all register values>)
```

3. read registers
```
IC.readRegs([<"ARRAY of REGISTER NAMES">])
```
4. write registers
```
IC.sendRegs({ <REGISTERNAME>:[<Values to write>] })
```